# DevOps

To develop a Machine Learning (ML) web application using GitLab and Flask Framework,  incorporating the 3 DevOps best practices mentioned in the Background section. This is an  open-ended assessment.

## Dataset

[Kaggle: Health-insurance](https://www.kaggle.com/datasets/mirichoi0218/insurance/code?datasetId=13720&sortBy=voteCount)


## Get the app running locally

1. Change into the `food-vision` directory
2. Create and activate a virtual environment (call it what you want, I called mine "env") For Windows

   ```
   pip install virtualenv
   virtualenv <ENV-NAME>
   Windows OS:
   env\Scripts\activate
   MAC OS:
   source <ENV-NAME>/bin/activate
   ```
3. Install the required dependencies (Sklearn, etc)

   ```
   pip install -r requirements.txt
   ```
